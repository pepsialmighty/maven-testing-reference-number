package fi.vamk.studentid;

public class ReferenceValidator {
  public static void main(String[] args) {
    System.out.println("Hello World!");
    System.out.println(calculateRefNumber("12345"));
  }

  public static String calculateRefNumber(String num) {
    try {
      char[] numChar = num.toCharArray();
      String result = "";
      float resTmp = 0;
      int divider = 0;
      if (numChar.length <= 18) {
        char[] mul = "137137137137137137".toCharArray();

        for (int i = 0; i < numChar.length; i++) {
          resTmp += (mul[i] - 48) * (numChar[i] - 48);
        }

        divider = (int) Math.ceil(resTmp / 10) * 10;
        result = divider - (int) resTmp + "";
      }
      return num.concat(result);
    } catch (Exception e) {
      // TODO: handle exception
      return e.getMessage();
    }
  }

  public static String getActualCheckingNumber(String num) {
    char[] numChar = num.toCharArray();
    String result = "";
    float resTmp = 0;
    int divider = 0;
    if (numChar.length <= 18) {
      char[] mul = "137137137137137137".toCharArray();

      for (int i = 0; i < numChar.length; i++) {
        resTmp += (mul[i] - 48) * (numChar[i] - 48);
      }

      divider = (int) Math.ceil(resTmp / 10) * 10;
      result = divider - (int) resTmp + "";
    }
    return result;
  }

}